# Andy Jones

<andy@bitmeta.co.uk> • <https://bitbucket.org/andyjones> • <https://github.com/andyjones>

## Employment

### Feb 2023 to current - Senior DevOps Engineer, Lodestone Security ([lodestone.com](https://lodestone.com/))

 * Designed a passive Attack Surface Monitoring service and API that performs 4,500 scans per day
 * Reduced the 99th percentile scan time from 10 minutes to 2 minutes by reworking constraints
 * Created a golang port checker to verify open ports
 * Implemented rate limiting proxy in golang to improve scan reliability
 * Terraform infrastructure in AWS including proactive monitoring using Cloudwatch Alarms
 * Deployment pipeline using Github Actions. Quality checks include highlighting breaking API changes on the PR
 * POC to search scan corpus and provide dashboards
 * Technologies: FastAPI, Python, Golang, Terraform, Github Actions, AWS Fargate, Cloudwatch and Log Insights

### Jan 2023 to Feb 2023 - Senior DevOps Engineer, Shiftlab.ai

Company closed shortly after joining due to lack of funding

### Feb 2018 to Nov 2022 - KPMG UK LLP

#### Internal Product Capability Lead, Infrastructure

 * Responsible for 3 service teams across KPMG's Tax, Audit, Pensions and Deal Advisory areas
 * Lead 20 people (10 promotions, coaching 4 first time team leads)
 * Delivered 30 new services for internal engagements
 * Supporting ~130 services for internal client engagements
 * Oversaw over 100 cloud environments across AWS and Azure
 * Recruiting to grow the Cloud team from 40 people to 110 including a 10 person offshore team
 * Personal project: developed a portal to provide clients with transparency on their monthly bill

#### Engineering Team Lead, Infrastructure

 * Responsible for KPMG's cloud shared services including the Atlassian stack, Jenkins and TeamCity
 * Kickstarted the new DevSecOps team to improve security & operational excellence
 * Technologies: Python, Terraform, Jenkins, Azure DevOps, TeamCity, Octopus Deploy, AWS, Azure

### Jul 2016 to Feb 2018 - Senior DevOps Engineer, WeSwap.com Ltd ([www.weswap.com](https://www.weswap.com))

 * Running 73 microservices written in Node and PHP across 49 servers
 * Automated server provisioning and maintenance using Puppet for 40 servers
 * Reduced provisioning new services from 3 hours and requiring Rackspace's help to 15 minutes and done by the developer
 * Replaced Ansible Tower with a custom deployment dashboard written in Elixir saving on the Tower license and providing more customisation
 * Setup a centralised logging server using Fluentd so developers didn't need to log in to all servers to view logs
 * Introduced nginx to proxy between Rackspace load balancers and all http services for visible access logging and performance and offload performance
 * Documented institutional knowledge, adding checklists and how to guides for all infrastructure
 * Technologies: NodeJS, PHP, Puppet, Python, MySQL, Redis, Fluentd, Ubuntu, Jenkins, Rackspace

<div class="page-break"></div>

### Jan 2006 to Jul 2016 - Broadbean Technology Ltd ([www.broadbean.com](https://www.broadbean.com))

 * Broadbean offers recruitment software as a service to recruitment agencies and corporate recruiters including Manpower and Michael Page

#### Senior Dev / Infrastructure Team

 * Maintained 60 physical servers in 2 datacentres and 120 instances in AWS
 * Added authentication to a Flask (python) deployment dashboard using TDD
 * Extracted several legacy CGI-based products from an SVN monolith into Git and modernised them to use the Mojolicious (perl) web framework – improving performance, simplifying the code base and reducing development cost of new features
 * Redesigned legacy report service to run across multiple servers to remove single points of failure. I used Puppet to provisioned the new service
 * Replaced LVS-based load balancer with HA Proxy to ensure HTTPS connections are evenly distributed across the backend webservers
 * Technologies: Perl, Mojolicious, Moo, Python, Puppet, Nginx, MySQL, Ubuntu, Git, Logstash, ElasticSearch, HAProxy, AWS (S3, EC2, RDS, ELB, Route 53), Jenkins

#### Tech Lead / Search Team

 * Designed and implemented Broadbean’s federated CV search product and API. Broadbean Search lets recruiters search multiple CV databases and job boards from a single place
 * Improved screen scraping feeds to display a screen capture so non-technical staff can quickly identify the reasons behind any search failures for an increased turnaround in client tickets.
 * Oversaw redesign of Broadbean’s internal database product TalentSearch using ElasticSearch to provide fast search results and faceting to help recruiters refine their search results
 * Conceived and architected a CV preview feature so recruiters can preview CVs in the browser instead of opening up MS Word each time to save time. This feature quickly spread to Broadbean’s other products due to demand
 * Prototyped Broadbean Search v2 on a Hacker day so stakeholders could visualise the benefits. Kickstarted development of the production version
 * Technologies: Perl, Apache, MySQL, Ubuntu, XPath, Gearman, Tokyo Tyrant, MongoDB, ElasticSearch, SVN

#### Senior Dev / Tech Team

 * Core developer on Linux, Apache, MySQL, Perl, Qmail platform that has scaled from posting 10,000 adverts and processing 50,000 emails per day to 40,000 adverts and 400,000 emails
 * Researched and implemented custom email spam filtering that increased the number of emails processed from 8 emails per second to 300 emails per second
 * Localisation of Broadbean products, now available in 9 languages
 * Technologies: Perl, Apache, MySQL, DNS (Bind), Qmail, Ubuntu, CentOS, SVN

### Previous employment

 * Dec 2004 to Nov 2005 **Developer, Tamar.com Ltd ([www.tamar.com](http://www.tamar.com))**
 * Jul 2004 to Sep 2004 **Computational Chemist, Hays Personnel Services (contract GlaxoSmithKline PLC)**
 * Sep 2002 to Sep 2003 **Computational Chemist - Placement Student, GlaxoSmithKline PLC**

## Education

Sep 2000 to Jun 2004
**MSci Chemistry with Industrial Experience, 2:1, University of Bristol**

## Hobbies

I play the piano to relax. I am a keen golfer and usually go abroad once per year to explore new courses. I love the simplicity (not easy!) and ingenuity behind magic especially card tricks. I subscribed to many tech newsletters (devopsweekly is my favourite) and read lots of open source websites to keep up to date. I spend too much time checking Hacker News.
